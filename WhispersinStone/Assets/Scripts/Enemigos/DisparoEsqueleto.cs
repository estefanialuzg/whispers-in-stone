using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoEsqueleto : MonoBehaviour
{
    public Transform player_pos;
    public float speed;
    public float distancia_frenar;
    public float distancia_retroceder;

    public Transform punto_instancia;
    public GameObject flecha;
    private float tiempo_ultimoDisparo;

    void Start()
    {
        player_pos = GameObject.Find("PJ").transform;

    }

    void Update()
    {
        //movimiento
        #region
        if(Vector2.Distance(transform.position, player_pos.position) > distancia_frenar)
        {
            transform.position = Vector2.MoveTowards(transform.position, player_pos.position, speed * Time.deltaTime);

        }
        if(Vector2.Distance(transform.position, player_pos.position) < distancia_retroceder)
        {
            transform.position = Vector2.MoveTowards(transform.position, player_pos.position, -speed * Time.deltaTime);

        }
        if(Vector2.Distance(transform.position, player_pos.position) < distancia_frenar && (Vector2.Distance(transform.position, player_pos.position) > distancia_retroceder))
        {
            transform.position = transform.position;
#endregion
        //gira
        #region 
        if(player_pos.position.x > this.transform.position.x)
        {
            this.transform.localScale = new Vector2 (1, 1);
        }else
        {
            this.transform.localScale = new Vector2 (-1, 1);

        }
        #endregion
        //disparo
        #region 
        tiempo_ultimoDisparo += Time.deltaTime;
        if(tiempo_ultimoDisparo >=2)
        {
            Instantiate(flecha, punto_instancia.position, Quaternion.identity);
            tiempo_ultimoDisparo = 0;
        }
        #endregion
        
        }
    }
}
