using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public LayerMask groundLayer;
    public LayerMask rampLayer;
    public bool isGrounded;
    public bool isOnRamp;
    public CircleCollider2D detector;

    public bool isOnWallLeft;
    public bool isOnWallRight;
    public float placeholderFloat;
    // Update is called once per frame
        

    public void CheckGround()
    {
        RaycastHit2D hitground = Physics2D.Raycast(transform.position, Vector2.down, 0.33f, groundLayer);
        if (hitground.collider != null)
        {
            // The character is grounded
            isGrounded = true;
        }
        else
        {
            // The character is in the air
            isGrounded = false;
        }

    }
    public void CheckRamp()
    {
        RaycastHit2D hitramp = Physics2D.Raycast(transform.position, Vector2.down, 0.40f, rampLayer);
        if (hitramp.collider != null)
        {
            // The character is grounded
            isOnRamp = true;
        }
        else
        {
            // The character is in the air
            isOnRamp = false;
        }

    }
    public void CheckRight()
    {
        RaycastHit2D hitwall = Physics2D.Raycast(transform.position + Vector3.right * 0.01f, Vector2.right, 0.1f, groundLayer);
        if (hitwall.collider != null)
        {
            // The character is against a wall
            isOnWallRight = true;
        }
        else
        {
            // The character is not against a wall
            isOnWallRight = false;
        }
    }
    public void CheckLeft()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.left * 0.01f, Vector2.left, 0.2f, groundLayer);
        if (hit.collider != null)
        {
            // The character is against a wall
            isOnWallLeft = true;
        }
        else
        {
            // The character is not against a wall
            isOnWallLeft = false;
        }
    }
}
