using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Header("Components")]
    private PlayerInput playerInput; // Reference to the PlayerInput component
    private Rigidbody2D rb; // Reference to the Rigidbody2D component

    [Header("Movement")]
    [SerializeField] private float movementAcceleration; // The rate at which the player's velocity increases when moving
    [SerializeField] private float maxSpeed; // The maximum horizontal speed of the player
    [SerializeField] private float linearDrag; // The amount of drag applied to the player's movement when not moving
    [SerializeField] private float hDirection; // The current horizontal direction of the player's movement
    [SerializeField] private float jumpThrust; // The amount of force necessary to 
    public Vector2 currentvelocity; // The current velocity of the player
    public Vector2 position; // The current position of the player
    public float jump = 1;
    public Vector2 trueDirection;
    public float placeholderFloat;
    public bool idktbh;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); // Get the Rigidbody2D component
        playerInput = GetComponent<PlayerInput>(); // Get the PlayerInput component
        Vector2 jump = new Vector2(0, jumpThrust);
        jump = jump.normalized;
        Vector2 movement = (new Vector2(hDirection, 0f) * movementAcceleration);
    }

    // Update is called once per frame
    void Update()
    {
        position = transform.position; // Update the current position of the player
        currentvelocity = rb.velocity; // Update the current velocity of the player
        playerInput.CheckGround(); // Check if the player is on the ground
        playerInput.CheckLeft(); // Check if the player is facing a wall on the left
        playerInput.CheckRight(); // Check if the player is facing a wall on the right
        playerInput.CheckRamp();

        hDirection = GetInput().x; // Update the horizontal direction based on the user input
        Jump();
    }

    private void FixedUpdate()
    {
        MoveCharacter(); // Move the character based on the user input
        ApplyLinearDrag(); // Apply drag to the character's movement when not moving
        
    }

    // Transforms the inputs to a vector2
    private Vector2 GetInput()
    {
        return new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")); // Get the user input and return it as a vector2
    }

    // Moves the character based on the user input
    private void MoveCharacter()
    {
        // Adds a force to the character that gets stronger over time
        rb.AddForce(new Vector2(hDirection, 0f) * movementAcceleration);

        // Clamps the speed to the variable maxSpeed
        if (Mathf.Abs(rb.velocity.x) > maxSpeed)
        {
            rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);
        }
    }

    // Applies drag to the character's movement when not moving
    private void ApplyLinearDrag()
    {
        if (Mathf.Abs(hDirection) < 0.4f && playerInput.isGrounded == true)
        {
            rb.drag = linearDrag; // Apply drag to the character's movement
        }
        else
        {
            rb.drag = 0f; // Remove drag from the character's movement
        }
    }
    private void Jump()
    {
        if (playerInput.isGrounded == true && Input.GetButtonDown("Jump"))
        {
            jump = 1;
            playerInput.isGrounded = false;

            rb.AddForce(new Vector2(0f, jump * jumpThrust), ForceMode2D.Impulse);
            UnityEngine.Debug.Log(rb.velocity.y);
        }
        
    }

}
